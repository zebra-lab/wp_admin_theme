jQuery(function() {

	var $ = jQuery;

  $('#toolbar-item-administration-tray')
    .removeClass('toolbar-tray-horizontal')
    .addClass('toolbar-tray-vertical')
    .removeAttr('data-offset-top')
    .attr('data-offset-left');

  // $('.toolbar-toggle-orientation').css('display', 'none');

  if( document.querySelector('.panel') ) {
    openPanel();
  }


  function openPanel() {
      // panels collection
      var panels = $('.panel'),
          // lately will be appended in each panel
          btn   = '<button type="button" class="panel__button"></button>';

      // creating hidden description nodes
      for(var i = 0; i < panels.length; i++) {
        // collection of links in current panel
        var links = $('.list-group__link > a', panels[i]);

        // it can be many links
        for(var ii = 0; ii < links.length; ii++) {
          // current link
          var link = links[ii];
          // creating description node on current link
          var  descriptionNode = document.createElement('dd');
                descriptionNode.className = 'list-group__description';
                descriptionNode.innerHTML = link.title;
          // append description node (hidden)
          $(link).parent().after(descriptionNode);
        }
      }

      // creating and appending buttons which will catch clicks
      $('.panel__title', panels).prepend(btn);

      // onlick = open/close description
      $('.panel__button').click( function(event) {

        var panel = $(this).closest('.panel'),
            btn = $(this),
            open = panel.hasClass('open');

        // closing or opening descriptions of panel
        // with opening panel and button get 'open' class
        if( !open ) {
          panel.addClass('open');
          btn.addClass('open');
        } else {
          panel.removeClass('open');
          btn.removeClass('open');
        }

      });

    };

});
